target=variable io if if0 for0 for while dowhile fornested function vector1D vector2D0 vector2D file procedure_math procedure_calling procedure_loop procedure_simplest struct_fonction struct_gros struct_hard struct_procedure struct_profond struct_simple struct_tableaux struct_tableau_struct

#if defined TARGET_variable
#define TARGET variable
#define TARGET_FR variables
#endif

#if defined TARGET_io
#define TARGET io
#define TARGET_FR Entrées-Sorties
#endif

#if defined TARGET_if
#define TARGET if
#define TARGET_FR conditionnelles
#endif

#if defined TARGET_if0
#define TARGET if0
#define TARGET_FR conditionnelles simples
#endif

#if defined TARGET_for0
#define TARGET for0
#define TARGET_FR boucles for simples
#endif

#if defined TARGET_for
#define TARGET for
#define TARGET_FR boucles for
#endif

#if defined TARGET_fornested
#define TARGET fornested
#define TARGET_FR boucles for imbriquées
#endif

#if defined TARGET_while
#define TARGET while
#define TARGET_FR boucles while
#endif

#if defined TARGET_dowhile
#define TARGET while
#define TARGET_FR boucles dowhile
#endif

#if defined TARGET_function
#define TARGET function
#define TARGET_FR fonctions
#endif

#if defined TARGET_vector1D
#define TARGET vector1D
#define TARGET_FR tableaux
#endif

#if defined TARGET_vector2D0
#define TARGET vector2D0
#define TARGET_FR tableaux 2D simples
#endif

#if defined TARGET_vector2D
#define TARGET vector2D
#define TARGET_FR tableaux 2D
#endif

#if defined TARGET_file
#define TARGET file
#define TARGET_FR Fichiers et flux
#endif

#if defined TARGET_procedure_math
#define TARGET procedure_math
#define TARGET_FR Procédures et opérations mathématiques
#endif

#if defined TARGET_procedure_loop
#define TARGET procedure_loop
#define TARGET_FR Procédures et boucles
#endif

#if defined TARGET_procedure_calling
#define TARGET procedure_calling
#define TARGET_FR Procédures appelant une procédure
#endif

#if defined TARGET_procedure_simplest
#define TARGET procedure_simplest
#define TARGET_FR Procédures simples
#endif

#if defined TARGET_procedure
#define TARGET procedure
#define TARGET_FR Procédures
#endif

#if defined TARGET_struct_fonction
#define TARGET struct_fonction
#define TARGET_FR Structures 2
#endif

#if defined TARGET_struct_gros
#define TARGET struct_gros
#define TARGET_FR Structures 7
#endif

#if defined TARGET_struct_hard
#define TARGET struct_hard
#define TARGET_FR Structures 8
#endif

#if defined TARGET_struct_procedure
#define TARGET struct_procedure
#define TARGET_FR Structures 3
#endif

#if defined TARGET_struct_profond
#define TARGET struct_profond
#define TARGET_FR Structures 4
#endif

#if defined TARGET_struct_simple
#define TARGET struct_simple
#define TARGET_FR Structures 1
#endif

#if defined TARGET_struct_tableaux
#define TARGET struct_tableaux
#define TARGET_FR Structures 5
#endif

#if defined TARGET_struct_tableau_struct
#define TARGET struct_tableau_struct
#define TARGET_FR Structures 6
#endif

\title{Compréhension de programmes C++ (TARGET_FR)}
\description{Compréhension de programmes C++ (TARGET_FR)}
\language{fr}
\niveau{U1}
\author{Nicolas M. Thiéry}
\email{Nicolas.Thiery@u-psud.fr}
\format{html}

\integer{static=1}
\text{module_data=modules/devel/nthiery/test~coding~readingCppPrograms.fr/data/}
\text{programs=wims(lookup TARGET in data/static/index)}
\text{program=randomitem(\programs)}

\if{\static == 1}{
    \text{code=wims(record 0 of data/static/\program)}
}{
    \text{code=wims(record 0 of data/\program)}
    \text{code=wims(replace internal CI1 by \CI1 in \code)}
    \text{code=wims(replace internal CI2 by \CI2 in \code)}
    \text{code=wims(replace internal I1 by \I1 in \code)}
    \text{code=wims(replace internal I2 by \I2 in \code)}
    \text{code=wims(replace internal I3 by \I3 in \code)}
    \text{code=wims(replace internal I4 by \I4 in \code)}
}

\integer{height=wims(linecnt \code)+3}
\integer{heightpixels=10*\height}

\css{
<script type="text/javascript" src="scripts/js/edit_area/edit_area_full.js"></script>
<script type="text/javascript">
                editAreaLoader.init({
                        id: "wims_show"
                        ,start_highlight: true
                        ,allow_toggle: false
			,allow_resize: true
                        ,language: "fr"
                        ,syntax: "cpp"
			,min_height: 200
                        ,min_width: 300
                        ,is_editable:false
                        ,toolbar: ""
                        ,show_line_colors: false
                });
        </script>
}

% When already possible, compute/fetch the answer
\if{_input.cpp isin \program}{
    \text{answerstyle=text}
    \if{\static == 1}{
        \text{answer=wims(lines2words wims(record 0 of data/static/\program.answer))}
    }
}{
    \text{answerstyle=symtext}
    \if{\static == 1}{
        \text{answer=wims(lines2words wims(record 0 of data/static/\program.answer))}
    }{
      \text{\tmpfile=slib(oef/newfile,.ccp,code)}
      \text{answer=wims(lines2words wims(exec docker-compile-and-run \tmpfile))}
    }
}

\statement{
    \if{_input.cpp isin \program}{
        <p>Quel nombre entre 0 et 99 devrait saisir l'utilisateur pour que le programme C++ suivant affiche 42?</p>
    }{
        <p>Quel affichage exact produit le programme C++ suivant?</p>
        <p>Les espaces, tabulations et sauts de lignes dans la réponse sont considérés comme équivalents.</p>
    }

    <textarea id="wims_show" cols="80" rows="\height" name="wims_show" readonly="readonly">\special{tabs2lines \code}</textarea>
}

\answer{}{\answer}{type=\answerstyle}
