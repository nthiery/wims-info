#! /bin/sh

tar czf archive.tgz -C dir .
../secure_exec_server &
sleep 3
curl --form "archive=@archive.tgz" --form "cmd=./monscript" "http://127.0.0.1:8080/secure_exec"

killall secure_exec_server

