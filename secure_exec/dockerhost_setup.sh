#!/bin/bash

BASE_DOCKER_IMAGE=crosbymichael/build-essential

# Taken from https://docs.docker.com/engine/installation/linux/docker-ce/debian/#install-using-the-repository

sudo apt-get update

sudo apt-get install \
     apt-transport-https \
     ca-certificates \
     curl \
     gnupg2 \
     software-properties-common \
     super python3-flask

curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -

sudo apt-key fingerprint 0EBFCD88

sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/debian \
   $(lsb_release -cs) \
   stable"

sudo apt-get update
sudo apt-get install docker-ce

#sudo groupadd docker  # aready exists
#sudo service docker restart
sudo docker pull $BASE_DOCKER_IMAGE

sudo useradd www

sudo cp secure_exec_local secure_exec_server /usr/local/bin

sudo sh -c 'echo "secure_exec_local /usr/local/bin/secure_exec_local u=nobody g=docker www" >> /etc/super.tab'

# At boot time run the server as www

sudo sh -c '#!/bin/sh echo "su -c /usr/local/bin/secure_exec_server www" >> /etc/rc.local'

# TODO
