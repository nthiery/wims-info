# Secure execution of a command in a sandbox

This implements a web service allowing to execute a command in a
docker sandbox with reasonable security.

## Usage of the client

This assumes that the webservice is running and its url is set in
[](secure_exec).

The following command uses the webservice to create a new sandbox,
initialize it with a copy of the content of `dir` in the sandbox, run
`cmd` in it, and print the output:

    secure_exec <dir> <cmd>

## Installation of the webservice

This assumes that you have access to some remote machine running some
variant of debian, under a user with sudo permission.

- Edit the [](Makefile) to point DOCKERHOST to the ip address of the
  server and DOCKERHOSTADMIN to the user

- Run:

     make dockerhost-preinstall
     make dockerhost-run

- (optional) Run

     make dockerhost-test

## Updating the webservice

Run:

    make dockerhost-update

## Tests

The directory `tests` contains a collection of tests scripts:

- Test 1 assumes that docker is installed.

- Tests 2 and 3 assume in addition that `secure_exec_local` has been
  installed in `/usr/local/bin` and `/etc/super.tab` has been
  configured locally as in `super.tab`.

- Test 4 assumes that the web service is running on some (remote)
  server, and that its url is set in `secure_exec`.

In addition, the following command runs some tests on the webservice:

    make dockerhost-test

## TODO

- More documentation

- Security review

- Truncation of output

- Authentication between client and server
  A minima: firewall setup to restrict incoming connections from the client

- Resource limitations

- The created temporary archive should be in a temporary directory and cleaned up
  Or could we avoid the temporary file?
